package com.example.consumingservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ConsumingServiceApplication {

	// registrador para enviar a saida para o console
	private static final Logger log = LoggerFactory.getLogger(ConsumingServiceApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ConsumingServiceApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Bean
	public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
		return args -> {
			CatFact catfact = restTemplate.getForObject(
					"https://catfact.ninja/fact", CatFact.class);
			log.info(catfact.toString());
		};
	}
}
